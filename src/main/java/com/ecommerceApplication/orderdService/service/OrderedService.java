package com.ecommerceApplication.orderdService.service;

import com.ecommerceApplication.orderdService.VO.Customer;
import com.ecommerceApplication.orderdService.VO.OrderedCustomerVO;
import com.ecommerceApplication.orderdService.entity.Ordered;
import com.ecommerceApplication.orderdService.repository.OrderedRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class OrderedService {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private OrderedRepository orderedRepository ;

    public Ordered saveOrdered(Ordered ordered)
    {
        log.info("saveOrdered method of OrderedService");
        return orderedRepository.save(ordered);
    }

    public OrderedCustomerVO getOrderedWithCustomer(Long id) {

        OrderedCustomerVO vo=new OrderedCustomerVO();
        Ordered ordered=orderedRepository.findByOrderedId(id);
        Customer customer=restTemplate.getForObject("http://CUSTOMER-SERVICE/customer/"+ordered.getCustomerId(),Customer.class);
        vo.setCustomer(customer);
        vo.setOrdered(ordered);
        return vo;




    }


}
