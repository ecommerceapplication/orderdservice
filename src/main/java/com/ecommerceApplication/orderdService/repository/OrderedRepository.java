package com.ecommerceApplication.orderdService.repository;

import com.ecommerceApplication.orderdService.entity.Ordered;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderedRepository extends JpaRepository<Ordered,Long>
{

    Ordered findByOrderedId(Long id);
}
