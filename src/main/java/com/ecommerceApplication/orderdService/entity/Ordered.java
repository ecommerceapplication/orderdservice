package com.ecommerceApplication.orderdService.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Ordered {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderedId;
    private String orderedAddress;
    private Date orderedDate;
    private Long customerId;
}
