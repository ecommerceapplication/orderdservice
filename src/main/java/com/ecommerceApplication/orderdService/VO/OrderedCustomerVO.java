package com.ecommerceApplication.orderdService.VO;

import com.ecommerceApplication.orderdService.entity.Ordered;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderedCustomerVO {
    private Ordered ordered;
    private Customer customer;
}
