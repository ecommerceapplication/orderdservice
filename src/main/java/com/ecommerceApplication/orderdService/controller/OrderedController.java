package com.ecommerceApplication.orderdService.controller;

import com.ecommerceApplication.orderdService.VO.OrderedCustomerVO;
import com.ecommerceApplication.orderdService.entity.Ordered;
import com.ecommerceApplication.orderdService.service.OrderedService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/ordered")
public class OrderedController {
    @Autowired
    private OrderedService orderedService;
    @PostMapping("/")
    public Ordered saveOrdered(@RequestBody Ordered ordered)
    {
        log.info("saveOrdered method of OrderedController");
      return  orderedService.saveOrdered(ordered);
    }
    @GetMapping("/{id}")
    public OrderedCustomerVO getOrderedWithCustomer(@PathVariable("id") Long id)
    {
        log.info("saveOrdered method of getOrderedWithCustomer");
        return orderedService.getOrderedWithCustomer(id);
    }
}
